# /usr/bin/env python
# encodig:utf-8

import random
import time
import os

def vecinos2(lista2, matriztemp):
    contador2 = 0
    for i in range(N):
        for j in range(N):
            #Si una posicion en la matriz lista2 es igual a 0 entra
            if (lista2[i][j] == 1):
                contador2 = 0
                if(i>=0 and i<N):
                    if(j>=0 and j<N):
                        for a in range(i-1,i+1):
                            for b in range(j-1,j+1):
                                #Si hay un valor 1 entonces sumara en un contador
                                if (lista2[a][b] == 1):
                                    contador2 += 1
                        #Si el contador es distinto de 3 y 2 entonces este valor 1 se cambiara por un 0
                        if (contador2 != 3 and contador2 != 2):
                            lista2[i][j] = 0
                            # Se guarda en la matriz temporal
                            matriztemp[i][j] = lista2[i][j]


def vecinos(lista2, matriztemp):
    contador = 0
    for i in range(N):
        for j in range(N):
            # Si una posicion en la matriz lista 2 es igual a 0 entra
            if (lista2[i][j] == 0):
                contador = 0
                if(i>=0 and i<N):
                    if(j>=0 and j<N):
                        for k in range(i-1,i+1):
                            for l in range(j-1,j+1):
                                # Si vale 0 este se sumara a un contador
                                if (lista2[k][l] == 0):
                                    contador += 1
                        # Si el contador es igual a 3 entonces la posicion se cambiara por un 1
                        if (contador == 3):
                            lista2[i][j] = 1
                            # Se guarda en la matriz temporal
                            matriztemp[i][j] = lista2[i][j]
                            # Si no pasara a la segunda funcion
    else:
        vecinos2(lista2, matriztemp)


# Main


# Se definen la variable lista2
lista2 = []
contador3 = 0
# Se pide un ingresar un valor para la matriz cuadrada
N = int(input("Ingrese valor matriz: "))
# Se forma la matriz con la lista 2 sobre la lista
for i in range(N):
    lista = []
    for i in range(N):
        lista.append(random.randrange(2))
    lista2.append(lista)
# Se pone una matriz temporal y se iguala a la lista(matriz)
matriztemp = lista2
# Se imprime la matriz random
for i in range(N):
    for j in range(N):
        print("|  ", lista2[i][j], end='')
    print("|\n")
# Se iguala las posiciones de la matriz temporaal a la de la matiz lista2
for i in range(N):
    for j in range(N):
        matriztemp[i][j] = lista2[i][j]

# Se llama a la funcion vecinos
while True:
    vecinos(lista2,matriztemp)
    contador3 += 1
    print("Generaciones creadas: ", contador3)
    print("\n")
    # Se imprime la matriz final con las condiciones
    for i in range(N):
        for j in range(N):
            print("|  ", matriztemp[i][j], end='')
        print("|\n")
    time.sleep(1)
    lista2 = matriztemp
    os.system("clear")
