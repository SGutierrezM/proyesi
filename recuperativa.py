# /usr/bin/env python
# encodig:utf-8


#este programa trata sobre datos de las flores: primero se tienen que mostrar
#los distintos tipos de especies que hay, luego el ancho y largo de los petalos,
#tambien lo mismo del sepalo, y luego separar por especies y guardar los datos
#en un archivo json

import json


def open_file():

    with open("iris.json", "r") as file:
        dic = json.load(file)

    return dic


#funcion que busca que no hayan nombres iguales de especies de flores
def busca(lis_dic,lista):

    for i in range(len(lista)):
        if lis_dic == lista[i]:
            return True
    return False
#funcion que muestra las especies que hay en el archivo
def especies():

    ma = open_file()
    dic = {}
    lista = []


    for i in range(len(ma)):
        dic =  ma[i]
        lis_dic = list(dic.values())
        for j in range(len(lis_dic)):
            comparar = busca(lis_dic[4],lista)
            if comparar is False:
                lis.append(lis_dic[4])

    for j in range(len(lista)):
        print("Especie: ", lista[j])




def comparador(dic):

    lis_dic = list(dic.values())
    for i in lis_dic:
        print(lis_dic)




#funcion que ve el ancho de los petalos
def ancho():

    ma = open_file()
    dic = {}
    lista = []
    


    for i in range(len(ma)):
        dic = ma[i]



        caso = comparador(dic)
#funcion que ve el largo de los petalos
def largo():

    ma = open_file()
    dic = {}
    lista = []



    for i in range(len(ma)):
        dic = ma[i]



        caso = comparador(dic)










def menu():
    dic = {}
    while True:
        print("Presione a para ver las especies de las flores")
        print("Presione b para calcular el promedio del ancho de los petalos")
        print("Presione c para calcular el promedio del largo de los petalos")
        print("Presione d para calcular la medida maxima del sepal")



        opcion = input("Ingrese la opcion: ")

        if opcion.upper() == "A":
            especies()

        elif opcion.upper() == "B":
            ancho()

        elif opcion.upper() == "C":
            largo()

        elif opcion.upper() == "D":
            maxima()

        elif opcion.upper() == "S":
            salir()


if __name__ == "__main__":

    menu()
