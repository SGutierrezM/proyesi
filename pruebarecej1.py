# /usr/bin/env python
# encodig:utf-8

#Programa que encripta las letras de las palabras ingresadas, moviendose n cantiad
#segun indique el usuario.


#Funcion que recibe el abecedario, las palabras y el desplazamiento
# y que forma una nueva palabra encriptada dependiendo de cuanto sea el desplazamiento
# y de la palabra ingresada
def encriptar(letras_may,letras_min,oracion,desplazamiento):


    x = ""
# a la nueva palabra le sumara la palabra previamente ingresada, pero aplicandole
# el desplazamiento formando la nueva palabra.
    for i in oracion:
        if i in letras_may:

            x = x + letras_may [letras_may.index(i) + desplazamiento%(len(letras_may))]

        elif i in letras_min:

            x = x + letras_min [letras_min.index(i) + desplazamiento%(len(letras_min))]

        else:
# si es un simbolo se copiara igual
            x = x + i


    print("El texto original es: ", oracion)
    print("El texto cifrado es: ", x)



def main():

    oracion = input("Ingrese oracion: ")
    desplazamiento = int(input("Ingrese valor de desplazaminto: "))

    letras_may = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
          'P','Q','R','S','T','U','V','W','X','Y','Z']

    letras_min = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o',
          'p','q','r','s','t','u','v','w','x','y','z']
    encriptar(letras_may,letras_min,oracion,desplazamiento)


if __name__=='__main__':
    main()
