# /usr/bin/env python
# encodig:utf-8

#programa que permite con un menu ingresar, editar, eliminar, leer o buscar aminoacidos en un diccionario
#comparando con otro diccionario ya hecho que contiene los aminoacidos y sus formulas quimicas, para que 
#se puedan agregar correctamente.

import json


def save_file(dic):
    with open("aminoacidos.json", 'w') as file:
        json.dump(dic, file)


def load_file():
    with open("aminoacidos.json", 'r') as file:
        dic = json.load(file)

    return dic

def load_second_file():
    with open ("aminoacidos_correctos.json", 'r') as file:
        dic = json.load(file)
    return dic

def editar():
#Esta funcion edita la estructura del aminoacido encontrado en el diccionario
    dic = load_file()
    print(dic)
    amino = input("Ingrese el aminoacido: ")
    
    permite = buscar(dic,amino)
    if permite is False:
        print("No existe")

    if permite:
        estruc = input("Ingrese la formula a editar: ")
        dic[amino] = estruc
        print(dic)
        save_file(dic)

def leer():
#permite leer el diccionario
    dic = load_file()
    print(dic)

def eliminar():
#permite eliminar una key con su value del diccionario
    dic = load_file()
    print(dic)
    elimi = input("Escriba el aminoacido a eliminar: ")

    permite = buscar(dic,elimi)
    if permite is False:
        print("No existe")
        
    if permite:

            del dic[elimi]
            print(dic)
            save_file(dic)


def comparar(lista_de_ami,letras):
    #compara las letras ingresadas en buscador con los aminoacidos del diccionario 
    i=0
    contador = 0
    for j in range(len(lista_de_ami)):
        if(i == 3 and contador == len(letras)):
            return True

        if letras[i] == lista_de_ami[j] and i == 0:
            i = i + 1
            contador = contador + 1

        elif i !=0 and letras[i] != lista_de_ami[j]:
            return False

        elif i != 0 and letras[i] == lista_de_ami[j]:
            contador = contador + 1
            i = i + 1
    if contador == len(letras):
        return True
    else:
        return False


def nuevo_buscador(lista_busqueda,busqueda_de_letras):

#transforma los valores ingresados en el  buscador en listas
    lista_de_aminos = []
    valores = []
    for i in range(len(lista_busqueda)):
        lista_de_aminos.append(list(lista_busqueda[i]))
        letras = list(busqueda_de_letras)

    for j in range(len(lista_busqueda)):
        valores.append(comparar(lista_de_aminos[j],letras))

    return valores


def buscador():
#permite ingresar una cadena de letras para poder buscarlas en el diccionario 
    dic = load_file()
    lista_de_busqueda = list(dic.values())
    print(lista_de_busqueda)
    busca_letras = input("porfavor, ingresar las letras a encontrar")
    listado = nuevo_buscador(lista_de_busqueda,busca_letras.upper())

    if len(listado) == 0:
        print("no son iguales")

    else:

        print("Los aminoacidos son:")
        i = 0
        for key,value in dic.items():
            if listado[i] == True:
                print(key)

            i = i + 1





def buscar(dic, amino):

    comprobar2 = dic.get(amino, False)

    return comprobar2



def lista(amino):

    dic2 = load_second_file()

    comprobar = dic2.get(amino.upper(), False)

    return comprobar


def ingresar():

    dic = load_file()

    while True:

        amino = input("Ingrese el nombre: ")
         #al ingresar el aminoacido , este se envia a la funcion comparar para ver si esta en el archivo con todos los aminoacidos 
        comparar = lista(amino)
        #compara el aminoacido ingresado con los previamente ingresados para ver si son iguales
        permite = buscar(dic,amino)
        
        print(comparar)
        if comparar is False:
            print("No existe")
            break

        dic2 = load_second_file()
        formula_buena = dic2[amino.upper()]

        if not permite:
            
            formula = input("Ingrese la formula:")
            if formula.upper() == formula_buena:
                ban = formula.upper()
                dic[amino] = ban
            else:
                print("Es incorrecto")
            if comparar is not False:
                break
        else:
            print("El aminoacido {} ya se encuentra".format(amino))
            break

    save_file(dic)
    print(dic)
    return dic




def menu():
    dic = {}
    while True:
        print("presione:")
        print("b para buscar")
        print("e para eliminar")
        print("i para insertar")
        print("l para leer el archivo")
        print("c para editar")
        print("s para salir")
        opcion = input("Ingrese una opcion: ")

        if opcion.upper() == "I":
            ingresar()

        elif opcion.upper() == "B":
            buscando()

        elif opcion.upper() == "C":
            editar()
            
        elif opcion.upper() == "E":
            eliminar()
            
        elif opcion.upper() == "L":
            leer()
            
        elif opcion.upper() == "S":
            quit()

        else:
            print("La opcion seleccionada no es valida, porfavor intentar de nuevo")
            pass


if __name__ == "__main__":

    menu()
