# /usr/bin/env python
# encodig:utf-8

#Este programa trata sobre abrir un archivo csv que contiene un top 50 de spotify
#junto con otros datos como la duracion o la popularidad, y a partir de ello se pide
#buscar algunos datos como la mediana y al final pasar los datos del archivo csv a un json




import json
import csv




def open_archivo():
    with open("top50.csv",'r') as csv_file:
        dic = list(csv.reader(csv_file))
    return dic

def open_archivo2():
    with open("top50.json",'r') as file:
        dic = json.load(file)
    return dic

def save_archivo(dic):
    with open("top50.json", 'w') as file:
        json.dump(dic, file)

#busca en la lista de nombres para que no se repitan
def busca(ar,artistas):

    for i in range(len(artistas)):
        if ar == artistas[i]:
            return True
    return False

#Funcion que da una lista con los artistas sin repetirse ninguno
def artistas():

    ar = open_archivo()
    artistas = []
    for i in range(len(ar)):
        if i == 0:
            artistas.append(ar[i][2])

        else:
            comparador = busca(ar[i][2],artistas)
            if comparador == False:
                artistas.append(ar[i][2])

    for i in range(len(artistas)):
        print(artistas[i])

    print("el numero de artistas es: ", (len(artistas)-1))
    #Fue puesto asi debido al tiempo
    print("El artista con mas aparaciones es: ", (ar[4][2]))

#Funcion que lista los datos pedidos, como el ranking, cancion,cantante y genero
#que sean iguales al valor de la media de decibelios
def listar():

    ar = open_archivo()
    listar = []
    for i in range(len(ar)):
        if ar[i][7] == '-6':
            listar.append(ar[i][0:4])
    print(listar)


#Se ordena la lista y se saca la mediana
# tambien se usa un valor bajo(-100) para que sea el primero en el sort, luego
# con el sort se cambia ya que lee solo valores numericos.para volver a dejar el
#valor como era.
def calcular(ban,mediana):

    ban = mediana[0]

    mediana[0] = -100

    mediana.sort()

    mediana[0] = ban

    print(mediana)

    mid = int(len(mediana)/2)

    print("La mediana es: " , mediana[mid])

#funcion que empieza buscando la lista con decibelios que luego pasaran a otra
#funcion donde se calulara la mediana
def mediana():

    ar = open_archivo()
    mediana = []
    for i in range(len(ar)):
        mediana.append(ar[i][7])

    for i in range(len(mediana)):
        if i != 0:
            ban = int(mediana[i])
            mediana[i] = ban

    cal = calcular(ban,mediana)

#Funcion que busca la cancion mas bailada
#incompleto
def bailada():

    ar = open_archivo()
    valores = []
    for i in range(len(ar)):
        valores.append(ar[i][6])
        valores.sort()
    print(valores)

#funcion que busca si la cancion mas popular es la mas mas_ruidosa
#Incompleto
def ruidosa():

    ar = open_archivo()
    popular = []
    for i in range(len(ar)):
        popular.append(ar[i][13])
        popular.sort()
    print(popular)

#Funcion que guarda los datos en un json
def guardar():

    dic = {}
    save_archivo(dic)
    ar = open_archivo()
    ranking = []
    cancion = []
    artista = []
    genero = []
    beat = []
    length = []
    popularity = []

    for i in range(len(ar)):
        ranking.append(ar[i][0])
        cancion.append(ar[i][1])
        artista.append(ar[i][2])
        genero.append(ar[i][3])
        beat.append(ar[i][4])
        length.append(ar[i][10])
        popularity.append(ar[i][13])

    for i in range(len(ranking)):
        dic[ranking[i]] = cancion[i],artista[i],genero[i],beat[i],length[i],popularity[i]

    save_archivo(dic)

# funcion que lee el archivo previamente guardado en json
def leer():

    dic = open_archivo2()
    print(dic)

def menu():
    caf = {}
    while True:
        print("Para poder ver determinada informacion respecto al top 50 de spotify, presione: ")
        print("a para ver el listado de los artistas")
        print("b para ver la mediana de ruido de los artistas")
        print("c para ver el listado del ranking,cancion,artista y genero que pertenecen a la mediana")
        print("d para ver el genero musical mas bailable y las tres canciones mas lentas")
        print("e para ver si la cancion popular es la mas ruidosa")
        print("f para guardar la lista en un json")
        print("g para poder leer el json")
        print("h para salir")
        opcion = input("La opcion es: ")

        if opcion.upper() == "A":
            artistas()

        elif opcion.upper() == "B":
            mediana()

        elif opcion.upper() == "C":
            listar()

        elif opcion.upper() == "D":
            bailada()

        elif opcion.upper() == "E":
            ruidosa()

        elif opcion.upper() == "F":
            guardar()

        elif opcion.upper() == "G":
            leer()

        elif opcion.upper() == "H":
            salir()







if __name__ == "__main__":
    menu()
