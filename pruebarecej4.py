# /usr/bin/env python
# encodig:utf-8

#Programa que pide 3 listas con datos previamente ingresados por el usuario
#para formar una y concadenarla. Despues forma otra lista con los mismos numeros
#de la lista concadenada pero sin repetirse, para luego formar una nueva lista
# con valores random entre 1 y 100 en las posiciones donde haya numero par.

import random




#funcion que solo reemplaza los numeros pares de la lista(la sin repeticiones)
#pero no afectando al numero 0 ni a los que terminan en el.
def ran(eliminar):

    for i in range(len(eliminar)):
        if eliminar[i]%2 == 0 and eliminar[i]%10 != 0 and eliminar[i] is not 0:
            eliminar[i] = random.randint(1,100)
    print(eliminar)



#funcion que recibe la lista concadenada , para formar una nueva donde los valores
# no se repitan
def repetidos(e):

    lista = []
    for i in range(len(e)):
        if i + 1 < len(e):
#si es diferente se agrega
            if e[i] != e[i+1]:
                lista.append((i))
#sino se elimina
            else:
                e.pop(e[i])
    return e


#Funcion que concadena la lista formada por las 3 listas con datos ingresados
def concadenar(d):

    conca = []

    for i in range(len(d)):

        conca += d[i]

        conca.sort()

    return conca


#funcion que pide al usuario 3 numeros, para formar una lista
def matriz(g):

    while True:

        g.append(int(input("Ingrese numeros para la lista : ")))
        g.append(int(input("Ingrese numeros para la lista : ")))
        g.append(int(input("Ingrese numeros para la lista : ")))
        break
    return g



def main():
#se forman listas y se les pasa a la funcion matriz para rellenarlas.
    a = []
    matriz(a)
    print(a)
    b = []
    matriz(b)
    print(b)
    c = []
    matriz(c)
    print(c)
    d = []
# se forma una  sola lista con las 3 listas en su interior
    for i in range(len(a)):
        if i + 2  < len(a):
            d.append(a)
    for i in range(len(b)):
        if i + 2 < len(b):
            d.append(b)
    for i in range(len(c)):
        if i + 2 < len(c):
            d.append(c)

    print(d)
    e = concadenar(d)
    print(e)
    eliminar = repetidos(e)
    print(eliminar)
    ran(eliminar)



if __name__=='__main__':
    main()
