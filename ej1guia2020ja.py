# /usr/bin/env python
# encodig:utf-8

#programa que a partir de un diccionario con su key y su value(aminoacido y su formula quimica)
#tiene que cambiar la key por el value y viceversa, ademas de separar la formula quimica de a dos
#y formando un nuevo diccionario con cada una de estas keys de dos elementos con el nombre del aminoacido value


dic = {'Histidine': 'C6H9N3O2'}

nuevo = {}

#ciclo que recorre todas las keys y values del diccionario
for key,value in dic.items():
    print(value,key)
    i = 0
    #sucedera hasta que recorra todo el largo del value
    while i < len(value):
        nuevo[value[i:i+2]] = key
        i = i + 2
print(nuevo)


#en primera instancia habia lo habia hecho asi
#dic = {'Histidine': 'C6H9N3O2'}
# nuevo = {}
# formula = dic.get("Histidine")
#for i in dic:
# print(i)
#nuevo[formula] = i
#print(nuevo)
